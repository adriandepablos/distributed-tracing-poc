package com.adrian.ms2_sleuth_zipkin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
@RestController
public class Service02Controller
{

    @RequestMapping("/")
    public String index() {
        log.info("[BEGIN] Calling controller of Service 02");
        String result = UUID.randomUUID().toString();
        log.info("Result: " + result);
        log.info("[END] Called controller of Service 02");
        return result;
    }
}

