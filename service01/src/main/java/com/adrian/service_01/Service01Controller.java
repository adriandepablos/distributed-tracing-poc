package com.adrian.service_01;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class Service01Controller
{
    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${service01.service02-location}")
    String service02Location;

    @RequestMapping("/")
    public void index() {
        log.info("[BEGIN] Calling controller of Service 01");
        log.info("Calling Service 02 from Service 01");
        String result = restTemplate.getForObject(service02Location, String.class);
        log.info("Response from Service 02: " + result);
        log.info("[END] Called controller of Service 01");
    }
}
