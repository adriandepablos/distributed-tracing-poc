1. Optional: check versions and ports in *.env* file.
1. In *docker-bind-volumes/elk/filebeat/beats-to-elasticsearch.conf*: comment and uncomment match pattern depending on whether there is *iv_user* field in logs.
1. In the *pom.xml* file of each service: comment and uncomment dependencies for Zipkin and Opentracing (Jaeger) depending on wich one we want to use.
1. Package the services with Maven.
1. Run containers:

        $ docker-compose -f docker-compose-elk.yml up -d
        $ docker-compose -f docker-compose-jaeger.yml up -d # Wait until Elasticsearch is up and running before executing this command.
        $ docker-compose -f docker-compose-zipkin.yml up -d
        $ docker-compose -f docker-compose-services.yml up -d # Wait until Jaeger is up and running before executing this command.
1. Make a request to service01: `$ curl -i http://localhost:48000`
1. See traces in Zipkin: access http://localhost:9411
1. See traces in Jaeger: access http://localhost:16686
1. See logs in Kibana:
  1. Access http://localhost:5601/
  1. Create index pattern: go to "Management" -> (Kibana) "Index Patterns" -> "Create index pattern":
      1. For the logs of the services, the pattern is: logstash-*
      1. Optional: for Zipkin traces, the pattern is: zipkin-*
      1. Optional: for Jaeger traces, the pattern is: jaeger-span-*
  1. Go to "Discover" and filter by trace, span, etc.